Functions changes summary: 0 Removed, 1 Changed, 0 Added function
Variables changes summary: 0 Removed, 0 Changed, 0 Added variable

1 function with some indirect sub-type change:

  [C] 'function int foo(type&)' at test3-v0.cc:10:1 has some indirect sub-type changes:
    parameter 1 of type 'type&' has sub-type changes:
      in referenced type 'struct type' at test3-v1.cc:9:1:
        type size changed from 64 to 96 (in bits)
        1 base class insertion:
          struct base at test3-v1.cc:2:1

